//var locationBtnHtml = '<a href="#" class="spm spm_s_cadastral" title="접속위치">접속위치</a>';
var initPosition = new naver.maps.LatLng(37.5666805, 126.9784147);
var map = new naver.maps.Map('map', {
    center: initPosition,
    zoom: 10,
    mapTypeId: naver.maps.MapTypeId.NORMAL
});

var markerOptions = {
    position: initPosition,
    map: map,
    icon: {
        url: './img/marker_bike.png',
        size: new naver.maps.Size(50, 39),
        origin: new naver.maps.Point(0, 0),
        anchor: new naver.maps.Point(20, 25)
    }
};

var marker = new naver.maps.Marker(markerOptions);

//customControl 객체를 이용하기
/*var customControl = new naver.maps.CustomControl(locationBtnHtml, {
    position: naver.maps.Position.RIGHT_CENTER
});
customControl.setMap(map);*/

var infowindow = new naver.maps.InfoWindow();

function onSuccessGeolocation(position) {
    var myLocation = new naver.maps.LatLng(position.coords.latitude, position.coords.longitude);
    var viewAddr = "";

    naver.maps.Service.reverseGeocode({
        location: myLocation
    }, function(status, response) {
        if (status !== naver.maps.Service.Status.OK) {
            return alert('Something wrong!');
        }

        var result = response.result, // 검색 결과의 컨테이너
            items = result.items; // 검색 결과의 배열

        viewAddr = items[1].address; // do Something
        //infowindow.setContent('<div style="padding:10px;">' + viewAddr + '</div>');
    });

    map.setCenter(myLocation); // 얻은 좌표를 지도의 중심으로 설정합니다.
    map.setZoom(10); // 지도의 줌 레벨을 변경합니다.

    //infowindow.setContent('<div style="padding:20px;">' + 'geolocation.getCurrentPosition() 위치' + '</div>');
    //infowindow.open(map, myLocation);

    marker.setPosition(myLocation);
    console.log('Coordinates: ' + myLocation.toString());
}

function onSuccessWatchPosition(position) {
    var myLocation = new naver.maps.LatLng(position.coords.latitude, position.coords.longitude);
    var viewAddr = "";

    naver.maps.Service.reverseGeocode({
        location: myLocation
    }, function(status, response) {
        if (status !== naver.maps.Service.Status.OK) {
            return alert('Something wrong!');
        }

        var result = response.result, // 검색 결과의 컨테이너
            items = result.items; // 검색 결과의 배열

        viewAddr = items[1].address; // do Something
        //infowindow.setContent('<div style="padding:10px;">' + viewAddr + '</div>');
    });

    map.setCenter(myLocation); // 얻은 좌표를 지도의 중심으로 설정합니다.
    marker.setPosition(myLocation);
}

function onErrorGeolocation(error) {
    var center = map.getCenter();

    infowindow.setContent('<div style="padding:20px;">' +
        '<h5 style="margin-bottom:5px;color:#f00;">Geolocation failed!</h5>'+ "latitude: "+ center.lat() +"<br />longitude: "+ center.lng() +"<br />errorCode: " + error.code +'</div>');

    infowindow.open(map, center);
}

/*var domEventListener = naver.maps.Event.addDOMListener(customControl.getElement(), 'click', function() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(onSuccessGeolocation, onErrorGeolocation);
    } else {
        var center = map.getCenter();
        infowindow.setContent('<div style="padding:20px;"><h5 style="margin-bottom:5px;color:#f00;">Geolocation not supported</h5></div>');
        infowindow.open(map, center);
    }
});*/

$(window).on("load", function() {
    var watch_Options= {
        enableHighAccuracy: true,
        maximumAge        : 30000,
        timeout           : 27000
    }

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(onSuccessGeolocation, onErrorGeolocation);
        navigator.geolocation.watchPosition(onSuccessWatchPosition, onErrorGeolocation, watch_Options);
    } else {
        var center = map.getCenter();
        infowindow.setContent('<div style="padding:20px;"><h5 style="margin-bottom:5px;color:#f00;">Geolocation not supported</h5></div>');
        infowindow.open(map, center);
    }
});