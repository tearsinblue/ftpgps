package com.ftp.map.testmapgps;

import android.os.AsyncTask;
import com.google.gson.Gson;
import java.util.Map;

public class Task extends AsyncTask<Map<String, String>, Integer, String> {

    public static String ip = "192.168.200.167"; // 자신의 IP주소를 쓰시면 됩니다.

    @Override
    protected String doInBackground(Map<String, String>... maps) {

        // Http 요청 준비 작업

        HttpClient.Builder http = new HttpClient.Builder
                ("POST", "http://" + ip + ":80/spring_mybatis/mapgps"); //포트번호,서블릿주소

        // Parameter 를 전송한다.
        http.addAllParameters(maps[0]);

        //Http 요청 전송
        HttpClient post = http.create();
        post.request();

        // 응답 상태코드 가져오기
        int statusCode = post.getHttpStatusCode();

        // 응답 본문 가져오기
        String body = post.getBody();

        return body;
    }

    @Override
    protected void onPostExecute(String s) {

        Gson gson = new Gson();
        Mapgps data = gson.fromJson(s, Mapgps.class);

        System.out.println("위도 : " + data.latitude);
        System.out.println("경도 : " + data.longitude);
    }
}
